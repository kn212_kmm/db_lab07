create table test_index(
id int not null,
pole1 char(36) not null,
pole2 char(216) not null
)

select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'test_index')

insert into test_index
values(1, 'a','b')

declare @i as int=31
while @i<240
begin
	set @i = @i+1;
	insert into test_index
		values(@i, 'a','b')
	end;

select index_type_desc,  index_depth, index_level, page_count,
record_count, avg_page_space_used_in_percent, 
	avg_fragmentation_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'torg_firm'), OBJECT_ID(N'dbo.test_index'), Null,
 Null, 'Detailed')

 truncate table test_index

create clustered index idx_cl_id on test_index(id)

declare @i1 as int=0
while @i1<18630
begin
	set @i1 = @i1+1;
	insert into test_index
		values(@i1, 'a','b')
	end;

insert into test_index
values(18631, 'a','b') 


truncate table test_index
declare @i2 as int=0
while @i2<8906
begin
	set @i2 = @i2+1;
	insert into test_index
		values(@i2%100, 'a','b')
	end;

insert into test_index
values(8909%100, 'a','b')

truncate table test_index
drop index idx_cl_id on test_index
create clustered index idx_cl_pole1 on test_index(pole1)


declare @i3 as int=0
while @i3<9000
begin
	set @i3 = @i3+1;
	insert into test_index
		values(@i3, format(@i3, '0000'),'b')
	end;

truncate table test_index
declare @i4 as int=0
while @i4<9000
begin
	set @i4 = @i4+1;
	insert into test_index
		values(@i4, cast(newid() as char(36)),'b')
	end;

alter index idx_cl_pole1 on test_index rebuild