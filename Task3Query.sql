drop index idx_cl_pole1 on test_index
 
create nonclustered index idx_ncl_pole1 on test_index(pole1)
truncate table test_index

declare @i as int=0
while @i<24472
begin
	set @i = @i+1;
	insert into test_index
		values(@i, format(@i, '0000'),'b')
	end;
--���� �� 3 ���� �������. �������
--���� �� ������� 1 �� ����� �� � 1 ��������. �������
--������� ����� ������� 3
--����������� �� ����� 98-75-65-1%
--������������ 25-28-0-0
-- � ���� �� 1 ������� �������� ������� ������
insert into test_index
		values(24473, '000024473','b')

drop index idx_cl_id on test_index

 create clustered index idx_cl_pid1 on test_index(id)
 create nonclustered index idx_ncl_pole2 on test_index(pole1)
 truncate table test_index

declare @i as int=0
while @i<28864
begin
	set @i = @i+1;
	insert into test_index
		values(@i, format(@i, '0000'),'b')
	end;
	
--3 �����. ������� �� 6 ����.
-- 1-3 �� 4-6 ����. ������� ������� ����� �������, ���������� ����� ����� 3
--������� ����� ������� 3
--����������� �� ����� 98-51-1-78-62-1%
--������������ 1-66-0-22-100-0
--������ ������� �� 3 ����

insert into test_index
values(28865, '000028865','b')


select * from test_index

select index_type_desc,  index_depth, index_level, page_count,
record_count, avg_page_space_used_in_percent, 
	avg_fragmentation_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'torg_firm'), OBJECT_ID(N'dbo.test_index'), Null,
 Null, 'Detailed')

 alter index idx_ncl_pole1 on test_index rebuild
 alter index idx_cl_pid1 on test_index rebuild

--3 �����. ������� �� 5 ����.
--����� 2 ����. ������� ����� ������� 2, ���� 3
--����������� �� ����� 97-26-1-99-78-63-1%
--������������ 1-66-0-0-0-22-100-0
--������ ������� �� 3 ����