SELECT sysobjects.name AS �������, sysindexes.name AS ������, sysindexes.indid AS �����
FROM sysobjects INNER JOIN
 sysindexes ON sysobjects.id = sysindexes.id
WHERE (sysobjects.xtype = 'U') AND (sysindexes.indid > 0)
ORDER BY sysobjects.name, sysindexes.indid

--------------------------------------------------------------------------------------


create nonclustered index ix_date_rozm on zakaz(date_rozm)
with  fillfactor  = 70
alter index ix_date_rozm on zakaz reorganize

select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'zakaz')

drop index ix_date_rozm on zakaz
---------------------------------------------------------------------------------------

drop index ix_klient_nazva_city on klient
create nonclustered index ix_klient_nazva_city on klient(city, nazva)

--------------------------------------------------------------------------------------

drop index ix_klient_nazva_city1 on klient
create nonclustered index ix_klient_nazva_city1 on klient(nazva, city)

select index_type_desc,  index_depth, index_level, page_count,
record_count, avg_page_space_used_in_percent, 
	avg_fragmentation_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'torg_firm'), OBJECT_ID(N'klient'), Null,
 Null, 'Detailed')
 --г����� �� � �������� �� � ��������� ���� �� ��������

 ---------------------------------------------------------------------------------------

 alter table zakaz drop constraint FK__zakaz__id_sotrud__300424B4
alter table sotrudnik drop constraint PK__sotrudni__668829F18FF35F1C

--------------------------------------------------------------------------------------

set statistics time on
set statistics io on

--������� ����, �� ���������������� � �����

create nonclustered index ix_tovar_nazva on tovar(Nazva)
SELECT id_tovar, Nazva, price FROM tovar WHERE Nazva = '������' 
drop index ix_tovar_nazva on tovar

create nonclustered index ix_tovar_price_nazva on tovar(Nazva, Price)
SELECT id_tovar, Nazva, price FROM tovar WHERE Nazva = '������' AND Price = 10 
drop index ix_tovar_price_nazva on tovar

create nonclustered index ix_tovar_id on tovar(id_tovar)
SELECT nazva FROM zakaz_tovar, tovar WHERE tovar.id_tovar = tovar.id_tovar
drop index ix_tovar_id on tovar

create nonclustered index ix_tovar_price on tovar(id_tovar, Price)
SELECT nazva FROM zakaz_tovar, tovar WHERE tovar.id_tovar = tovar.id_tovar
AND Price >10
drop index ix_tovar_price_id on tovar
--��� ������� 140��
--� ��������� 106��
create nonclustered index ix_tovar_all on tovar(id_tovar, Price, Nazva)
SELECT id_tovar, Nazva, price FROM tovar WHERE Nazva = '������' 
SELECT id_tovar, Nazva, price FROM tovar WHERE Nazva = '������' AND Price = 10 
SELECT nazva FROM zakaz_tovar, tovar WHERE tovar.id_tovar = tovar.id_tovar
SELECT nazva FROM zakaz_tovar, tovar WHERE tovar.id_tovar = tovar.id_tovar
AND Price >10
drop index ix_tovar_all on tovar
--� ����� �������� 85��
